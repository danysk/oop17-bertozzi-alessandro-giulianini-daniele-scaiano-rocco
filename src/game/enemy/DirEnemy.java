
package game.enemy;

/**
 * Class for the enemies's direction. {@link BasicEnemy} {@link BigEnemy} {@link BossEnemy} {@link FastEnemy} {@link SmartEnemy}
 * 
 */

public enum DirEnemy {
/**
 * Directions of the enemy: U = UP, L = LEFT, R = RIGHT, D = DOWN.
 */
RIGHT, LEFT, UP, DOWN, U_L, U_R, D_L, D_R;
}
